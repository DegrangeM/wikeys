# Wikeys

_Important : ce jeu de société n'a pas été réalisé par moi. Je ne fais que le repartager ici étant donné qu'il est disponible sous licence libre._

Wikeys est un jeu sérieux au format jeu de société permettant de faire découvrir les principes fondateurs de Wikipédia. Le jeu est très bien pensé, fun à jouer et les règles sont très simples (un mode avancé existe).

Un dossier pédagogique est également fourni pour aider les enseignants à tirer parti du projet.

Le jeu est disponible sous licence CC BY-SA 4.0 (voir mentions dans le fichier pdf).

## Plus d'informations

- https://www.wikimedia.fr/wikeys/

- https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Wikim%C3%A9dia_France/Wikeys
